import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {

  //String url = "http://198.15.77.50:8196/;";
  String url = "http://cloudstream2032.conectarhosting.com/9316/stream";

  AnimationController _animationController;
  bool isPlaying = true;
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 650));
    audioStart();
  }

  Future<void> audioStart() async {
    try {
      await FlutterRadio.audioStart();
      print('Audio Start OK');
    }catch(ex){
      print(ex.toString());
    }
  }

  _buildCard({
    Config config,
    Color backgroundColor = Colors.transparent,
    DecorationImage backgroundImage,
    double height = 152.0,
  }) {
    return Container(
      height: height,
      width: double.infinity,
      child:  WaveWidget(
          config: config,
          backgroundColor: backgroundColor,
          backgroundImage: backgroundImage,
          size: Size(double.infinity, double.infinity),
          waveAmplitude: 0,
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Bsxlab Radio',
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          appBar: new AppBar(
            title: const Text('Caracol Radio 91.3 FM'),
            backgroundColor: Colors.lightBlue,
            centerTitle: false,
          ),
          body: Container(
            color: Colors.white,
            child: new Column(
              children: <Widget>[
                Expanded(
                    flex: 4,
                    child: _buildCard(
                      height: 128.0,
                      backgroundImage: DecorationImage(
                        image: NetworkImage(
                          'https://test.besixplus.com/radio/logo_caracol.jpeg',
                        ),
                        fit: BoxFit.fitWidth,
                        colorFilter:
                        ColorFilter.mode(Colors.white, BlendMode.softLight),
                      ),
                      config: CustomConfig(
                        colors: [
                          Colors.white70,
                          Colors.orange[300],
                          Colors.white,
                          Colors.lightBlueAccent[100]
                        ],
                        durations: [18000, 8000, 5000, 12000],
                        heightPercentages: [0.85, 0.86, 0.88, 0.90],
                        blur: MaskFilter.blur(BlurStyle.solid, 10),
                      ),
                    ),
                ),
                Expanded(
                  flex: 2,
                  child: Align(
                      alignment: FractionalOffset.center,
                      child: IconButton(
                        color: Colors.lightBlue,
                        icon: AnimatedIcon(
                          icon: AnimatedIcons.play_pause,
                          progress: _animationController,
                        ),
                        iconSize:80,
                        onPressed: () => _handleOnPressed(),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ));
  }
  void _handleOnPressed() {
    isPlaying = !isPlaying;
    if (isPlaying){
      FlutterRadio.stop();
      _animationController.reverse();
    }else {
      FlutterRadio.play(url: url);
      _animationController.forward();
    }
  }
}